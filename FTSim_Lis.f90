subroutine coo_to_csr(N, nnz, COO_IA, COO_JA, COO_VAL, IA, JA, VAL)

        implicit none
#include "lisf.h"

        INTEGER :: N, nnz
        INTEGER, dimension(1:nnz) :: COO_IA, COO_JA
        REAL(kind = 8) , dimension(1:nnz) :: COO_VAL
        REAL(kind = 8) :: x1
        LIS_INTEGER, dimension(0:N) :: IA
        LIS_INTEGER, dimension(0:(nnz-1)) :: JA
        LIS_SCALAR,  dimension(0:(nnz-1)) :: VAL
        INTEGER :: i, j

        !---- Conversion vars ---!
        INTEGER :: k, k0, iad, ibgn, iend

        do k=1,N+1
                IA(k - 1) = 0
        enddo

        do k=1, nnz
                IA(COO_IA(k) - 1) = IA(COO_IA(k) - 1)+1
        enddo

        k = 0

        do j=1,N+1
                 k0 = IA(j - 1)
                 IA(j - 1)  = k
                 k = k+k0
        enddo

       do k=1, nnz
                i = COO_IA(k)
                j = COO_JA(k)
                x1 = COO_VAL(k)
                iad = IA(i - 1)
                VAL(iad) =  x1 
                JA(iad) = j - 1 
                IA(i - 1) = iad + 1
        enddo

        do j = N - 1, 0, -1
                IA(j + 1) = IA(j)
        enddo


        IA(0) = 0

        !do i = 0, N - 1

         !       ibgn = IA(i)
         !       iend = IA(i + 1) - 1
         !       do j = ibgn, iend
         !               print *, " I : J : ", i, JA(j), " val : ", val(j)
         !       enddo
        !enddo

end subroutine coo_to_csr


subroutine solve_lis(N, nnz, COO_IA, COO_JA, COO_VAL, R, work)

        IMPLICIT NONE
#include "mpif.h"
#include "lisf.h"


        INTEGER :: N, nnz
        INTEGER,  dimension(1:nnz) :: COO_IA, COO_JA
        REAL(kind = 8), dimension(1:nnz) :: COO_VAL
        LIS_INTEGER, allocatable, dimension(:) :: IA
        LIS_INTEGER, allocatable, dimension(:) :: JA
        LIS_SCALAR, allocatable, dimension(:) :: val
        LIS_SCALAR, dimension(1:N) :: R
        LIS_SCALAR, dimension(1:N) :: work

        INTEGER :: i, j 

        ! ----- LIS variables ---- !
        LIS_MATRIX A
        LIS_INTEGER ln, is, ie, iter, ierr        
        LIS_VECTOR b, x
        INTEGER , allocatable, dimension(:) :: idx
        LIS_SOLVER solver

        integer :: my_id, ibgn, iend , icol

        ALLOCATE(IA(0:N))
        ALLOCATE(JA(0:(nnz - 1)), VAL(0: (nnz - 1)))

        call lis_initialize(ierr)

!        call MPI_COMM_RANK(MPI_COMM_WORLD, my_id, ierr)
        call lis_matrix_create(0,A,ierr)
        call lis_matrix_set_size(A,0,N,ierr)
        call lis_matrix_get_size(A, ln, N, ierr)
        call lis_matrix_get_range(A, is, ie, ierr)

        call lis_matrix_set_type(A,LIS_MATRIX_CSR,ierr)

        call coo_to_csr(N, nnz, COO_IA, COO_JA, COO_VAL, IA, JA, VAL)
                
        call lis_matrix_set_type(A,LIS_MATRIX_CSR,ierr)


       call lis_matrix_set_csr(IA(ie - 1) - IA(is - 1), IA((is - 1) : (ie - 1)), JA(0:(nnz-1)), val(0:(nnz-1)), A, ierr)

        call CHKERR(ierr)

        call lis_matrix_assemble(A,ierr)
        call CHKERR(ierr)

        call lis_vector_duplicate(A, b, ierr)
        call lis_vector_get_range(A, is, ie, ierr)
        ALLOCATE(idx(is : (ie - 1)))

        do i = is , ie - 1
                idx(i) = i
        enddo
          
        
        call lis_vector_duplicate(A, x, ierr)

        CALL lis_solver_create(solver, ierr)

        call lis_solver_set_option("-i bicg -p ilu", solver, ierr)
        call lis_solver_set_option("-tol 1.0e-12", solver, ierr)

        call lis_vector_set_values(LIS_INS_VALUE, ie - is , idx(is : (ie - 1)), R((is ):(ie - 1)), b, ierr)
        call lis_vector_set_values(LIS_INS_VALUE, ie - is , idx(is : (ie - 1)), work((is ):(ie - 1)), x, ierr)

        call lis_solve(A, b, x, solver, ierr)

        call CHKERR(ierr)

        call lis_vector_gather(x, work, ierr)

end subroutine solve_lis
        

